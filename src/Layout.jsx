import { Outlet } from "react-router-dom";
import { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import Header from './components/Header/Header'
import Footer from "./components/Footer/Footer";
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'


function Layout() {

    const [modalToShow, setModalToShow] = useState({
        active: false,
        type: null
        
    });
    
    const [currentObj, setCurrentObj] = useState(null)

    useEffect(() => {
        fetch('products.json')
        .then(response => response.json())
        .then(data => setItems(data));
    }, [])

    const [cart, setCart] = useState([]);
    const [star, setStar] = useState([]);
    const [items, setItems] = useState([]);

    useEffect(() => {
        const storedCart = JSON.parse(localStorage.getItem('cart')) || [];
        const storedStar = JSON.parse(localStorage.getItem('star')) || [];
        const storedItems = JSON.parse(localStorage.getItem('items')) || [];

        setCart(storedCart);
        setStar(storedStar);
        setItems(storedItems);
    }, []);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        localStorage.setItem('star', JSON.stringify(star));
        localStorage.setItem('items', JSON.stringify(items));
    }, [cart, star, items]);

    useEffect(() => {
        star.map(id => {
            localStorage.setItem(`starClick-${id}`, 'true');
            return null;
        });
    }, [star]);

    function addToCart(product) {
        const {id}=product
            if (!cart.includes(id)) {
            const newCart = [...cart, id]
            setCart(newCart);
            }
    }

    function addToStar(id) {
        if (!star.includes(id)) {
            const newStar = [...star, id]
            setStar(newStar);
        }
        else {
            const newStar = star.filter(itemId => itemId !== id);
            setStar(newStar);
        }
    }

    function showModal(type, currentId) {
        setModalToShow({
            active: true,
            type: type
        });

    const currentProduct = items.find((product) =>
            product.id === currentId
        )
        setCurrentObj(currentProduct)
    }

    function removeFromCart(product) {
        const { id }=product
        setCart(prevCart => prevCart.filter(itemId => itemId !== id));
    };

    function removeFromFavorite(product) {
        const { id }=product
        setStar(prevStar => prevStar.filter(itemId => itemId !== id));
    };

    function closeModal() {
        setModalToShow({
            active: false,
            type: null
        });
        setCurrentObj(null)
    }

        return (
            <>
                <Header cart={cart} star={star} />
                <Outlet context={{ items, cart, star, setCart, setStar, addToCart, addToStar, showModal, removeFromCart, removeFromFavorite }} /> 
                {modalToShow.type === 'image' && (
                    <ModalImage
                    active={modalToShow.active}
                    setActive={closeModal}
                    firstText={"NO, CANCEL"}
                    secondaryText={'YES, DELETE'}
                    firstClick={closeModal}
                        secondaryClick={() => { removeFromCart(currentObj); removeFromFavorite(currentObj); closeModal() }}
                        productName={currentObj.name}
                        productImage={currentObj.imageUrl}
                    />
                )}
                {modalToShow.type === 'text' && (
                    <ModalText
                    active={modalToShow.active}
                    setActive={closeModal}
                    firstText={'ADD TO CART'}
                    isSecondModal={true}
                    firstClick={() => {addToCart(currentObj)}}
                    productName={currentObj.name}
                    productDescription={currentObj.description}
                    showModal={showModal}
                    />
                )}
                <Footer/>
            </>
    )
}

Layout.propTypes = {
    items: PropTypes.arrayOf(
    PropTypes.shape({
        id: PropTypes.number.isRequired,
        type: PropTypes.string,
    })
    ),
    cart: PropTypes.arrayOf(PropTypes.number),
    star: PropTypes.arrayOf(PropTypes.number),
};

Layout.defaultProps = {
    items: [],
    cart: [],
    star: [],
};

export default Layout;
