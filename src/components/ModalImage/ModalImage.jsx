import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody'
import './ModalImage.scss'


function ModalImage({active, setActive, firstText, secondaryText, firstClick, secondaryClick, productName, productImage}) {
    
    return (
        <>
            <Modal
                active={active}
                setActive={setActive}
                firstText={firstText}
                secondaryText={secondaryText}
                firstClick={firstClick}
                secondaryClick={secondaryClick}
                productName={productName}>
                <ModalBody>
                    <img className='modal-body__img' src={productImage}  alt="" />
                    <h2 className="modal-body__title modal-body__title_magin-first-modal">{productName} Delete!</h2>
                    <p className='modal-body__text modal-body__text_margin-first-modal'>By clicking the “Yes, Delete” button, {productName} will be deleted</p>
                </ModalBody>
            </Modal>
            
        </>
    )
}

export default ModalImage;