import Button from '../Button/Button';
import './ProductCard.scss'
import { useState, useEffect } from 'react';
import StarIcon from '../StarIcon/StarIcon';


function ProductCard(props) {

    const [starClick, setStarClick] = useState(false);

    useEffect(() => {
        const storedStarClick = localStorage.getItem(`starClick-${props.id}`);
        setStarClick(storedStarClick === 'true');
    }, [props.id]);

    function handleStarClick(event) {
        event.preventDefault();
        const newStarClick = !starClick;
        setStarClick(newStarClick);
        localStorage.setItem(`starClick-${props.id}`, newStarClick.toString());
        props.addToStar(props.id);
    }
 
        return (
            <div className="product">
                <a className={`product__link ${props.isFavoritePage ? 'favorite__link' : ''} ${props.isCartPage ? 'favorite__link' : ''}`} href="#">
                    <div className='product__image-container'>
                        <img className={`product__image ${props.isFavoritePage ? 'favorite__image' : ''} ${props.isCartPage ? 'favorite__image' : ''}`} src={props.imageUrl} alt="" />
                        
                        <div className="product__icon">
                            <StarIcon starClick={starClick} handleStarClick={handleStarClick} /> 
                        </div>
                    </div>
                    <div className={`${props.isFavoritePage ? 'favorite__info' : ''} ${props.isCartPage ? 'favorite__info' : ''}`}>
                        <div className='product__main-info'>
                        <h3 className="product__name">{props.name}</h3>
                    </div>
                    <div className='product__description'>
                        <div>
                            <p className="product__vendor-code">{props.sku}</p>
                            <p className="color">{props.color}</p>
                            <p className="product__price">{props.price} UAH</p>
                        </div>
                        <div>
                            {!props.hideAddToCartButton && (
                                <Button onClick={(event) => {
                                    event.preventDefault();
                                    props.showModal('text', props.id);
                            }} className='btn btn-card'>Add to cart</Button>)}
                            {props.isCartPage && (
                                <Button onClick={(event) => {
                                    event.preventDefault();
                                    props.showModal('image', props.id);
                            }} className='btn btn-card'>&times; Видалити </Button>)}  
                        </div>
                    </div>
                    </div>
                </a>
            </div>
        )
    }

export default ProductCard;
