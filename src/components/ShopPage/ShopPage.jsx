import ProductCard from "../ProductCard/ProductCard";
import './ShopPage.scss';


function ShopPage({ items, showModal, addToCart, addToStar }) {
    
    return (
        <>
            <div className="shop">
                {items.map((product) =>
                    <ProductCard
                        imageUrl={product.imageUrl}
                        name={product.name}
                        sku={product.sku}
                        color={product.color}
                        price={product.price}
                        key={product.id}
                        id={product.id}
                        description={product.description}
                        showModal={showModal}
                        addToCart={addToCart}
                        addToStar={addToStar}
                    />
                )
                }
        </div>
        </>
    )  
}

export default ShopPage;
