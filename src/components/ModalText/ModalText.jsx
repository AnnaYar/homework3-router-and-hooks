import Modal from '../Modal/Modal'
import ModalBody from '../ModalBody/ModalBody' 



function ModalText({active, setActive, firstText, firstClick, isSecondModal, productName, productDescription}) {

  return (
    <>
      <Modal
        active={active}
        setActive={setActive}
        firstText={firstText}
        firstClick={firstClick}
        isSecondModal={isSecondModal}
        productName={productName}>
        <ModalBody>
          <h2 className="modal-body__title modal-body__title_magin-second-modal">Add {productName}</h2>
          <p className='modal-body__text modal-body__text_margin-second-modal'>{productDescription}</p>
        </ModalBody>
      </Modal>
    </>
    )
}

export default ModalText;
