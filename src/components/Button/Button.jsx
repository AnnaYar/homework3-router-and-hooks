import React from "react";
import './Button.scss'


function Button({type="button", className, onClick, children}) {

    return (
        <button type={type} className={`btn ${className}`} onClick={onClick} >
            {children}
        </button>
    );
};

export default Button;