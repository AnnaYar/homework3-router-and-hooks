import ShopPage from "../components/ShopPage/ShopPage";
import { useOutletContext } from "react-router-dom";
import './ChildPage.scss'

function ChildPage() {
    const { items, addToCart, showModal, addToStar } = useOutletContext();

    return (
        <>
            <div className="container child-page">
                <h2 className="child-page__title">Дитина</h2>
                <ShopPage items={items} showModal={showModal} addToCart={addToCart} addToStar={addToStar}  />
            </div>

        </>
    )
}

export default ChildPage;